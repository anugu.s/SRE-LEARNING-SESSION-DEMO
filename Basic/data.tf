#------------------------------------------------------------
# data.tf
#------------------------------------------------------------

data "aws_iam_policy" "ec2-read-only" {
    arn = "arn:aws:iam::aws:policy/AmazonEC2ReadOnlyAccess"
}

data "aws_iam_policy_document" "ecs-tasks-role-assumption-document" {
  statement {
    effect = "Allow"

    principals {
      type = "Service"

      identifiers = [
        "ecs-tasks.amazonaws.com"
      ]
    }

    actions = [
      "sts:AssumeRole"
    ]
  }
}
