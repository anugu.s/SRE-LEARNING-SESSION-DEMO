# tfenv installation
brew install tfenv

# Install specific terraform version
tfenv install "version"

# Authenticate with AWS and get STS credentials
okta-aws chegg-exp-01-nonprod sts get-caller-identity

# Terraform Init
terraform init

# Terraform Plan
terraform plan

# Terraform Apply
terraform apply
