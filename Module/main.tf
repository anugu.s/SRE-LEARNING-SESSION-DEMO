#------------------------------------------------------------
# main.tf
#------------------------------------------------------------

module "demo-iam-role-creation" {
  source = "./module"
  iam_role_name = "demo-sre-learning-session"
}
