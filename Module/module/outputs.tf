#------------------------------------------------------------
# outputs.tf
#------------------------------------------------------------

output "iam_role_arn" {
  description = "ARN of the IAM role"
  value       = aws_iam_role.ec2-read-only-role.arn
}
