#------------------------------------------------------------
# main.tf
#------------------------------------------------------------

resource "aws_iam_role" "ec2-read-only-role" {
    name               = var.iam_role_name
    assume_role_policy = data.aws_iam_policy_document.ecs-tasks-role-assumption-document.json
}

resource "aws_iam_role_policy_attachment" "ec2_read_only_policy_attachment" {
    role       = aws_iam_role.ec2-read-only-role.name
    policy_arn = data.aws_iam_policy.ec2-read-only.arn
}
